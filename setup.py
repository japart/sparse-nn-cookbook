#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

print(requirements)

setup(
    name="sparse_nn_cookbook",
    version="0.0.1",
    description="Sparse neural network creator",
    license="BSD",
    keywords="",
    url="",
    packages=find_packages(),
    include_package_data=True,
    install_requires=requirements,
)
