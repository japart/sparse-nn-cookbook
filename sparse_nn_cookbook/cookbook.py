import torch
import pandas as pd
from enum import Enum

class LayerType(Enum):
    INP = "inp"
    HID = "hid"
    OUT = "out"

class Layer():
    """
    Gathered informations about layers

    Parameters
    ----------
    rec : dict
        Fragment of recipe with information about layer
    layer_type : LayerType
        Type of layer (input, hidden, output)
    """
    def __init__(self, rec, layer_type):
        self.recipe = rec
        self.idx = [i['idx'] for i in self.recipe]
        self.i = torch.LongTensor([list(range(len(self.recipe)))])
        self.bias = torch.sparse.FloatTensor(self.i, torch.FloatTensor([[i['bias']] for i in self.recipe])).to_dense()
        self.v = None
        self.tensor = None
        self.type = layer_type

class Connection():
    """
    Representation of the links between layers

    Parameters
    ----------
    rec : dict
        Fragment of recipe with information about created connections
    layer_from : Layer
        Object that contain input values
    layer_to : Layer
        Object where newly calculated values should be returned
    """
    def __init__(self, rec, layer_from, layer_to):
        self.recipe = rec
        self.connections = [[layer_from.idx.index(i['fromNeuron']),
                             layer_to.idx.index(i['toNeuron']),
                             i['weight']] for i in self.recipe]

        self.weights = torch.FloatTensor([i[2] for i in self.connections])
        self.coord = torch.LongTensor([[i[1], i[0]] for i in self.connections])
        self.tensor = torch.sparse.FloatTensor(self.coord.t(), self.weights)

class CookBook():
    """
    Generates sparse neural network from given 'recipe' and then runs it

    Parameters
    ----------
    recipe : dict
        JSON file with all variables needed for network generation

    Methods
    -------
    run
        Performs propagation of network based on provided inputs
    """

    def __init__(self, recipe):
        self.layer_inp = Layer(recipe['nn']['inpLayer'], LayerType.INP)
        self.layer_hid = Layer(recipe['nn']['hidLayer'], LayerType.HID)
        self.layer_out = Layer(recipe['nn']['outLayer'], LayerType.OUT)

        self.con_inp_hid = Connection(recipe['nn']['inpToHidConnections'], self.layer_inp, self.layer_hid)
        self.con_hid_out = Connection(recipe['nn']['hidToOutConnections'], self.layer_hid, self.layer_out)
        self.con_out_inp = Connection(recipe['nn']['recConnections'], self.layer_out, self.layer_inp)

    def run(self, input_vals, n):
        # Create input layer
        self.layer_inp.v = torch.FloatTensor(input_vals)
        self.layer_inp.tensor = torch.sparse.FloatTensor(self.layer_inp.i, self.layer_inp.v).to_dense()
        for i in range(n):
            self._forward()
            if i < n:
                # Do recurrent calculations if not last iteration
                self._calculate_next(self.layer_out, self.layer_inp, self.con_out_inp)

        return self.layer_out.tensor

    def _calculate_next(self, prev_layer, next_layer, connection):
        # Calculate next layer value
        next_input = torch.sparse.mm(connection.tensor, prev_layer.tensor)
        
        if prev_layer.type == LayerType.OUT and next_layer.type == LayerType.INP:
            # Add extra neuron to match inp tensor
            t = torch.tensor([[1]]).float()
            next_input = torch.cat((next_input, t), 0)
            if next_layer.recipe[0]['activFunc'] == 'tanh':
                next_layer.tensor = torch.tanh(torch.sub(next_input, next_layer.bias))
            elif next_layer.recipe[0]['activFunc'] == 'idem':
                next_layer.tensor = torch.sub(next_input, next_layer.bias)
            # Change T neuron value to 1
            next_layer.tensor[[-1]] = 1
        else:
            if next_layer.recipe[0]['activFunc'] == 'tanh':
                next_layer.tensor = torch.tanh(torch.sub(next_input, next_layer.bias))
            elif next_layer.recipe[0]['activFunc'] == 'idem':
                next_layer.tensor = torch.sub(next_input, next_layer.bias)
        

    def _forward(self):
        self._calculate_next(self.layer_inp, self.layer_hid, self.con_inp_hid)
        self._calculate_next(self.layer_hid, self.layer_out, self.con_hid_out)
