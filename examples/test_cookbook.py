import json
from sparse_nn_cookbook import CookBook

with open('../materials/api_response.json') as f:
    recipe = json.load(f)

cookbook = CookBook(recipe)

print(cookbook.run([[1], [1], [1], [1]], 1))
