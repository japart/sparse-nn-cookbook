import json

from sparse_nn_cookbook import Layer, Connection

with open('../materials/api_response.json') as f:
    recipe = json.load(f)

layer_inp = Layer(recipe['nn']['inpLayer'])

layer_hid = Layer(recipe['nn']['hidLayer'])

layer_out = Layer(recipe['nn']['outLayer'])

con_inp_hid = Connection(recipe['nn']['inpToHidConnections'], layer_inp, layer_hid)
con_hid_out = Connection(recipe['nn']['hidToOutConnections'], layer_hid, layer_out)
con_out_in = Connection(recipe['nn']['recConnections'], layer_out, layer_inp)
