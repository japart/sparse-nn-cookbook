# Sparse NN Cookbook

Sparse Neural Network Cookbook


## Installation

In order to install use: 

```bash
python setup.py install
```

To develop the module use:

```
python setup.py develop
```